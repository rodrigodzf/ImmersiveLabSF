import processing.video.*;
import codeanticode.syphon.*;
import oscP5.*;
import netP5.*;

PGraphics canvas;
SyphonServer server;
OscP5 oscP5;
NetAddress videoMapperAddress;

color bgColor = color(0, 0,0 );

PImage img;
int[] imgPos = { 10, 20 };
float[] imgScale = { 3.0, 1.0 };

Movie mov;
float[] movPos = { -900, -600 };
float[] movScale = { 1.9, 1.0 };

float ellipsePos[] = { 3280.0, 150 };
float ellipseSize[] = {800.0, 1200.0 };
color ellipseColor = color(0, 255, 0, 150.0);

float boxPos[] = { 4200.0, 360, 0.0 }; 
float boxSize[] = { 250.0, 250.0, 250.0 };
float boxColor = color(0, 0, 255, 255.0);

void setup() 
{
  size(1280, 180, P3D);
  canvas = createGraphics(5120, 720,P3D);
  server = new SyphonServer(this, "Processing_Syphon");
  
  oscP5 = new OscP5(this, 12000);
  videoMapperAddress = new NetAddress("127.0.0.1", 8400);
  
  OscMessage videoMapperMessage = new OscMessage("/SwitchSyphonClient");
  videoMapperMessage.add("panel_mapping");
  videoMapperMessage.add("Processing_Syphon");
  videoMapperMessage.add(1.0);
  oscP5.send(videoMapperMessage, videoMapperAddress);
  
  img = loadImage(sketchPath + "/image.jpg"); 
  
  mov = new Movie(this, sketchPath + "/Autofahrt-Complete_GEAR-MASTER-Short_Clean_2015-09-05.mp4");
//  mov = new Movie(this, sketchPath + "/fingers.mov");

  mov.loop();
}

void draw() 
{
  canvas.beginDraw();
  
  canvas.background( bgColor );

//  canvas.tint(255, 255);
//  canvas.image(img, imgPos[0], imgPos[1], imgScale[0] * img.width, imgScale[1] * img.height);
//  
  canvas.tint(255, 200);
  canvas.image(mov, movPos[0], movPos[1], movScale[0] * mov.width, movScale[1] * mov.height);
//  
//  canvas.noStroke();
//  canvas.fill(ellipseColor);
//  canvas.ellipse( ellipsePos[0], ellipsePos[1], ellipseSize[0], ellipseSize[1] );
//   
//  canvas.noFill();
//  canvas.stroke(boxColor);
//  canvas.pushMatrix();
//  canvas.translate(boxPos[0], boxPos[1], boxPos[2]);
//  canvas.box(boxSize[0], boxSize[1], boxSize[2]);
//  canvas.popMatrix();

  canvas.endDraw();
  
  server.sendImage(canvas);
//  image(canvas,0,0, width, height);
}

void movieEvent(Movie m) 
{
  m.read();
}
