#include "testApp.h"
#include "ofAppGlutWindow.h"

//--------------------------------------------------------------
int main(){
	ofAppGlutWindow window; // create a window
	// set width, height, mode (OF_WINDOW or OF_FULLSCREEN)
//	ofSetupOpenGL(&window, 4*1280, 720, OF_WINDOW);
//    ofGLESWindowSettings settings;
//    settings.width = 1024;
//    settings.height = 500;
//    settings.setGLESVersion(3);
//    ofCreateWindow(settings);
    
    ofSetupOpenGL(&window, 4*1280, 720, OF_WINDOW);
//    ofSetupOpenGL(&window, 1280, 720/4, OF_WINDOW);
	ofRunApp(new testApp()); // start the app
}
