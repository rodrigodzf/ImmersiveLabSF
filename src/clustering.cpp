//
//  clustering.cpp
//  ImmersiveLab_clustering
//
//  Created by Daniel Bisig on 15/11/15.
//
//

#include "clustering.h"
#include "testApp.h"

#pragma mark ClusteredPoint implementation

ClusteredPoint::ClusteredPoint()
: mTPoint(NULL)
, mCentroid(NULL)
, mColor(0, 0, 0, 255)
{}

ClusteredPoint::ClusteredPoint( point* pTPoint )
: mTPoint(pTPoint)
, mCentroid(NULL)
, mColor(0, 0, 0, 255)
{}

ClusteredPoint::ClusteredPoint( const ClusteredPoint& pCPoint )
: mTPoint(pCPoint.mTPoint)
, mCentroid(pCPoint.mCentroid)
, mColor(pCPoint.mColor)
{}

ClusteredPoint::~ClusteredPoint()
{}

ClusteredPoint&
ClusteredPoint::operator=( const ClusteredPoint& pCPoint )
{
    mTPoint = pCPoint.mTPoint;
    mCentroid = pCPoint.mCentroid;
    mColor = pCPoint.mColor;
    
    return *this;
}

#pragma mark Centroid implementation

float Centroid::sStablePositionCriteria = 0.005;
float Centroid::sMaxPointDistance = 0.005;
int Centroid::sMinLifeTime = 10;
int Centroid::sMaxDeathTime = 40;
float Centroid::sPositionSmoothing = 0.9;

Centroid::Centroid()
: mID(0)
, mXCoord(0.0)
, mYCoord(0.0)
, mPrevXCoord(0.0)
, mPrevYCoord(0.0)
, mLifeTime(0)
, mDeathTime(0)
, mStable(false)
, mColor(255, 0, 0, 150)
{}

Centroid::Centroid( float pXCoord, float pYCoord )
: mID(0)
, mXCoord(pXCoord)
, mYCoord(pYCoord)
, mPrevXCoord(0.0)
, mPrevYCoord(0.0)
, mLifeTime(0)
, mDeathTime(0)
, mStable(false)
, mColor(255, 0, 0, 150)
{}

Centroid::Centroid( const Centroid& pCentroid )
: mID(pCentroid.mID)
, mXCoord(pCentroid.mXCoord)
, mYCoord(pCentroid.mYCoord)
, mPrevXCoord(pCentroid.mPrevXCoord)
, mPrevYCoord(pCentroid.mPrevYCoord)
, mLifeTime(pCentroid.mLifeTime)
, mDeathTime(pCentroid.mDeathTime)
, mCPoints(pCentroid.mCPoints)
, mStable(pCentroid.mStable)
, mColor(pCentroid.mColor)
{}

Centroid::~Centroid()
{}

Centroid&
Centroid::operator=( const Centroid& pCentroid )
{
    mID = pCentroid.mID;
    mXCoord = pCentroid.mXCoord;
    mYCoord = pCentroid.mYCoord;
    mPrevXCoord = pCentroid.mPrevXCoord;
    mPrevYCoord = pCentroid.mPrevYCoord;
    mLifeTime = pCentroid.mLifeTime;
    mDeathTime = pCentroid.mDeathTime;
    mCPoints = pCentroid.mCPoints;
    mStable = pCentroid.mStable;
    mColor = pCentroid.mColor;
    
    return *this;
}

void
Centroid::display()
{
    ofSetColor(255, 0, 0, 255);
    ofSetLineWidth(4.0);
    ofSetCircleResolution(50);
    ofCircle( mXCoord*1280, mYCoord*720, 12);
    ofSetColor(255, 0, 0, 255);
    ofDrawBitmapString(ofToString(mID), mXCoord*1280+20, mYCoord*720+10);
}

#pragma mark CentroidGroup implementation

int CentroidGroup::sMaxCentroidCount = 10;
float CentroidGroup::sMaxMatchDistance = 0.06;

CentroidGroup::CentroidGroup()
: mAllClustersStable(false)
, mAllPointsClustered(false)
, mClusteringFinished(true)
, mMinXCoord(0.0)
, mMaxXCoord(0.0)
{}

CentroidGroup::CentroidGroup(float pMinXPos, float pMaxXPos, int pMinCentroidID, int pMaxCentroidID)
: mAllClustersStable(false)
, mAllPointsClustered(false)
, mClusteringFinished(true)
, mMinXCoord(pMinXPos)
, mMaxXCoord(pMaxXPos)
, mMinCentroidID(pMinCentroidID)
, mMaxCentroidID(pMaxCentroidID)
{
    for(int id=pMinCentroidID; id<=pMaxCentroidID; ++id) mAvailableCentroidIds.push_back(id);
}

CentroidGroup::CentroidGroup( const CentroidGroup& pCentroidGroup )
: mCentroids(pCentroidGroup.mCentroids)
, mTmpCentroids(pCentroidGroup.mTmpCentroids)
, mCPoints(pCentroidGroup.mCPoints)
, mAllClustersStable(pCentroidGroup.mAllClustersStable)
, mAllPointsClustered(pCentroidGroup.mAllPointsClustered)
, mClusteringFinished(pCentroidGroup.mClusteringFinished)
, mMinXCoord(pCentroidGroup.mMinXCoord)
, mMaxXCoord(pCentroidGroup.mMaxXCoord)
, mAvailableCentroidIds(pCentroidGroup.mAvailableCentroidIds)
{}

CentroidGroup::~CentroidGroup()
{}

CentroidGroup&
CentroidGroup::operator=( const CentroidGroup& pCentroidGroup )
{
    mCentroids = pCentroidGroup.mCentroids;
    mTmpCentroids = pCentroidGroup.mTmpCentroids;
    mCPoints = pCentroidGroup.mCPoints;
    mAllClustersStable = pCentroidGroup.mAllClustersStable;
    mAllPointsClustered = pCentroidGroup.mAllPointsClustered;
    mClusteringFinished = pCentroidGroup.mClusteringFinished;
    mMinXCoord = pCentroidGroup.mMinXCoord;
    mMaxXCoord = pCentroidGroup.mMaxXCoord;
    mAvailableCentroidIds = pCentroidGroup.mAvailableCentroidIds;
    
    return *this;
}

std::vector<Centroid>
CentroidGroup::getClusters() const
{
    while( mLock == true ) usleep(10);
    
    std::vector<Centroid> clusters;
    for(auto it=mCentroids.begin(); it != mCentroids.end(); ++it)
    {
        clusters.push_back(**it);
    }
    
    return clusters;
}

void
CentroidGroup::resetTouchPoints()
{
    mCPoints.clear();
}

void
CentroidGroup::addTouchPoint( point* pTPoint )
{
    if( mClusteringFinished == false ) return;
    
    ClusteredPoint cPoint(pTPoint);
    cPoint.mColor = ofColor(255, 0, 0, 255);
    mCPoints.push_back( cPoint );
}

void
CentroidGroup::calculateClusters()
{
    //std::cout << "CentroidGroup::calculateClusters() begin\n";
    
    if( mClusteringFinished == false ) return;
    
    if( mLock == true ) return;
    mLock = false;
    
    mClusteringFinished = false;
    mAllClustersStable = false;
    mAllPointsClustered = false;
    
    //std::cout << "mCentroids size " << mCentroids.size() << "\n";
    
    // increment death time of all existing centroids
    for (auto it = mCentroids.begin() ; it != mCentroids.end(); ++it)
    {
        (*it)->mDeathTime += 1;
    }
    
    mTmpCentroids.clear();
    
    //std::cout << "mCPoints size " << mCPoints.size() << "\n";
    
    if( mCPoints.size() > 0 )
    {
        //add first centroid
        {
            Centroid centroid( ofRandom( mMinXCoord, mMaxXCoord), ofRandom(0.0, 1.0) );
            centroid.mColor = ofColor( 255, 0, 0, 255);
            mTmpCentroids.push_back( centroid );
        }
        
        while( mAllClustersStable == false || mAllPointsClustered == false )
        {
            if( mAllClustersStable == false || mAllPointsClustered == false )
            {
                if(mAllClustersStable == true) // add new centroid
                {
                    Centroid centroid = Centroid( ofRandom(mMinXCoord, mMaxXCoord), ofRandom(0.0, 1.0) );
                    centroid.mColor = ofColor( 255, 0, 0, 255 );
                    mTmpCentroids.push_back( centroid );
                }
                
                updateCentroids();
                
                if( mAllClustersStable == true && mAllPointsClustered == true )
                {
                    // assign lowest point id to cluster
                    for (auto c_it = mTmpCentroids.begin() ; c_it != mTmpCentroids.end(); ++c_it)
                    {
                        Centroid& centroid = *c_it;
                        int pointCount = centroid.mCPoints.size();
                        
                        if( pointCount > 0 )
                        {
                            int lowestId = 10000;
                            int currentId;
                            
                            for (auto p_it = centroid.mCPoints.begin() ; p_it != centroid.mCPoints.end(); ++p_it)
                            {
                                ClusteredPoint& cPoint = *p_it;
                                point* tPoint = cPoint.mTPoint;
                                currentId = tPoint->ID;
                                
                                if( currentId < lowestId ) lowestId = currentId;
                            }
                            
                            centroid.mID = lowestId;
                        }
                        else
                        {
                            centroid.mID = -1;
                        }
                    }
                }
            }
        }
    }
    
    
    //std::cout << "tmpCentroids size " << mTmpCentroids.size() << "\n";
    
    // update centroids based on new centroids
    std::list< Centroid* > unmatchedCentroids;
    for(auto cIT=mCentroids.begin(); cIT!=mCentroids.end(); ++cIT) unmatchedCentroids.push_back( *cIT );
    std::list< Centroid > unmatchedTmpCentroids;
    Centroid* minMatchCentroid;
    float minMatchDistance = 10000.0;
    float currentMatchDistance;
    
    // find distance based closest match between new centroids and existing centroids
    for( auto tmpCIT= mTmpCentroids.begin(); tmpCIT != mTmpCentroids.end(); ++tmpCIT )
    {
        minMatchDistance = 10000.0;
        minMatchCentroid = NULL;
        Centroid& tmpCentroid = *tmpCIT;
        
        for(auto cIT= mCentroids.begin(); cIT != mCentroids.end(); ++cIT)
        {
            Centroid* centroid = *cIT;
            
            currentMatchDistance = (centroid->mXCoord - tmpCentroid.mXCoord) * (centroid->mXCoord - tmpCentroid.mXCoord) + (centroid->mYCoord - tmpCentroid.mYCoord) * (centroid->mYCoord - tmpCentroid.mYCoord);
            
            if(currentMatchDistance < minMatchDistance )
            {
                minMatchDistance = currentMatchDistance;
                minMatchCentroid = centroid;
            }
        }
        
        if( minMatchDistance <= sMaxMatchDistance && minMatchCentroid != NULL )
        {
            
            
            Centroid* centroid = minMatchCentroid;
            
            unmatchedCentroids.remove(centroid);
            
            float positionSmoothing;
            if( centroid->mLifeTime <= Centroid::sMinLifeTime ) positionSmoothing = 0.0;
            else positionSmoothing = Centroid::sPositionSmoothing;
            
            centroid->mPrevXCoord = centroid->mXCoord;
            centroid->mPrevYCoord = centroid->mYCoord;
            centroid->mXCoord = centroid->mPrevXCoord * positionSmoothing + tmpCentroid.mXCoord * (1.0 - positionSmoothing);
            centroid->mYCoord = centroid->mPrevYCoord * positionSmoothing + tmpCentroid.mYCoord * (1.0 - positionSmoothing);
            centroid->mLifeTime += 1;
            centroid->mDeathTime = 0;
            
            if (centroid->mLifeTime == 0){
                centroid->mOnset = true;
            }else{
                centroid->mOnset = false;
            }
        }
        else
        {
            unmatchedTmpCentroids.push_back(tmpCentroid);
        }
    }
    
    // release existing centroids that have not been matched
    for(auto cIT=unmatchedCentroids.begin(); cIT != unmatchedCentroids.end(); ++cIT)
    {
        Centroid* centroid = *cIT;
        
        centroid->mDeathTime += 1;
        
        if(centroid->mDeathTime > Centroid::sMaxDeathTime)
        {
            mCentroids.remove(centroid);
            mAvailableCentroidIds.push_back(centroid->mID);
            
            delete centroid;
        }
    }
    
    // create centroids for unmatched new centroids
    for(auto tCIT=unmatchedTmpCentroids.begin(); tCIT!=unmatchedTmpCentroids.end(); ++tCIT)
    {
        Centroid& tmpCentroid = *tCIT;
        Centroid* centroid = new Centroid( tmpCentroid.mXCoord, tmpCentroid.mYCoord );
        
        centroid->mPrevXCoord = centroid->mXCoord;
        centroid->mPrevYCoord = centroid->mYCoord;
        centroid->mLifeTime = 0;
        centroid->mDeathTime = 0;
        
        centroid->mID = mAvailableCentroidIds.front();
        mAvailableCentroidIds.pop_front();
        
        mCentroids.push_back(centroid);
    }
    
    mClusteringFinished = true;
    
    mLock = false;
}

void
CentroidGroup::updateCentroids()
{
    //std::cout << "updateCentroids begin\n";
    
    mAllPointsClustered = true;
    mAllClustersStable = true;
    
    //std::cout << "mTmpCentroids size " << mTmpCentroids.size() << "\n";
    
    // reset points stored in centroids
    for (auto it = mTmpCentroids.begin() ; it != mTmpCentroids.end(); ++it)
    {
        Centroid& centroid = *it;
        centroid.mCPoints.clear();
    }
    
    // assign points to centroids
    float distance;
    float minDistance;
    Centroid* minCentroid;
    
    //std::cout << "assign points to centroids\n";
    
    for (auto p_it = mCPoints.begin() ; p_it != mCPoints.end(); ++p_it)
    {
        ClusteredPoint& cPoint = *p_it;
        point* tPoint = cPoint.mTPoint;
        
        //std::cout << "tPoint " << tPoint->ID << " x " << tPoint->x << " y " << tPoint->y << "\n";
        
        minDistance = 10000.0;
        
        for (auto c_it = mTmpCentroids.begin() ; c_it != mTmpCentroids.end(); ++c_it)
        {
            Centroid& centroid = *c_it;
            
            //std::cout << "centroid " << " x " << centroid.mXCoord << " y " << centroid.mYCoord << "\n";
            
            distance = (centroid.mXCoord - tPoint->x) * (centroid.mXCoord - tPoint->x) + (centroid.mYCoord - tPoint->y) * (centroid.mYCoord - tPoint->y);
            
            //std::cout << "distance " << distance << "minDistance " << minDistance << "\n";
            
            if( distance < minDistance )
            {
                minDistance = distance;
                minCentroid = &centroid;
            }
        }
        
        minCentroid->mCPoints.push_back(cPoint);
        
        //std::cout << "minDistance " << minDistance << " sMaxPointDistance " << Centroid::sMaxPointDistance << "\n";
        //std::cout << "minCentroid " << " x " << minCentroid->mXCoord << " y " << minCentroid->mYCoord << " pointCount " << minCentroid->mCPoints.size() << "\n";
        
        if( minDistance < Centroid::sMaxPointDistance )
        {
            cPoint.mCentroid = minCentroid;
        }
        else if( mTmpCentroids.size() < sMaxCentroidCount )
        {
            mAllPointsClustered = false;
            cPoint.mCentroid = NULL;
        }
    }
    
    
    // update position of centroid
    float centroidPositionChange[2];
    
    for(auto c_it = mTmpCentroids.begin(); c_it != mTmpCentroids.end(); ++c_it)
    {
        Centroid& centroid = *c_it;
        
        //std::cout << "centroid " << " x " << centroid.mXCoord << " y " << centroid.mYCoord << " pointCount " << centroid.mCPoints.size() << "\n";
        
        if( centroid.mCPoints.size() > 0 )
        {
            float oldCentroidPosition[] = { centroid.mXCoord, centroid.mYCoord };
            
            centroid.mXCoord = 0.0;
            centroid.mYCoord = 0.0;
            
            for(auto p_it=centroid.mCPoints.begin(); p_it != centroid.mCPoints.end(); ++p_it)
            {
                ClusteredPoint& cPoint = *p_it;
                point* tPoint = cPoint.mTPoint;
                
                //std::cout << "tPoint " << tPoint->ID << " x " << tPoint->x << " y " << tPoint->y << "\n";
                
                centroid.mXCoord += tPoint->x;
                centroid.mYCoord += tPoint->y;
            }
            
            centroid.mXCoord /= (float)centroid.mCPoints.size();
            centroid.mYCoord /= (float)centroid.mCPoints.size();
            
            // calculate centroid movement distance
            centroidPositionChange[0] = oldCentroidPosition[0] - centroid.mXCoord;
            centroidPositionChange[1] = oldCentroidPosition[1] - centroid.mYCoord;
            
            //std::cout << "centroid avg x " << centroid.mXCoord << " y " << centroid.mYCoord << " ch x " << centroidPositionChange[0] << " y " <<centroidPositionChange[1] << "\n";
            
            if( ( centroidPositionChange[0] ) * ( centroidPositionChange[0] ) + ( centroidPositionChange[1] ) * ( centroidPositionChange[1] ) < Centroid::sStablePositionCriteria )
            {
                centroid.mStable = true;
            }
            else
            {
                centroid.mStable = false;
                mAllClustersStable = false;
            }
        }
        else
        {
            centroid.mXCoord = ofRandom(mMinXCoord, mMaxXCoord);
            centroid.mYCoord = ofRandom(0.0, 1.0);
            
            centroid.mStable = false;
            mAllClustersStable = false;
        }
    }
    
    //std::cout << "updateCentroids end\n";
}

void
CentroidGroup::display()
{
    if( mLock == true ) return;
    mLock = true;
    
    for(auto it = mCentroids.begin(); it != mCentroids.end(); ++it)
    {
        Centroid* centroid = *it;
        
        centroid->display();
    }
    
    mLock = false;
}