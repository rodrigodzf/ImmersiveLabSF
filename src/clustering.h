/** \file
 //  clustering.h
 //  ImmersiveLab_clustering
 //
 //  Created by Daniel Bisig on 15/11/15.
 */

#ifndef _clustering_h_
#define _clustering_h_

#include <iostream>
#include <vector>
#include <mutex>
#include "ofMain.h"

struct point;
class Centroid;

#pragma mark ClusteredPoint definition

class ClusteredPoint
{
    friend class CentroidGroup;
    
public:
    ClusteredPoint( point* pTPoint );
    ClusteredPoint( const ClusteredPoint& pCPoint );
    ~ClusteredPoint();
    
    ClusteredPoint& operator=( const ClusteredPoint& pCPoint );
    
protected:
    ClusteredPoint();
    
    point* mTPoint;
    Centroid* mCentroid;
    ofColor mColor;
};

#pragma mark Centroid definition

class Centroid
{
    friend class CentroidGroup;
    
public:
    Centroid();
    Centroid( float pXCoord, float pYCoord );
    Centroid( const Centroid& pCentroid );
    ~Centroid();
    
    Centroid& operator=( const Centroid& pCentroid );
    
    void display();
    
protected:
    static float sStablePositionCriteria;
    static float sMaxPointDistance;
    static int sMinLifeTime;
    static int sMaxDeathTime;
    static float sPositionSmoothing;
    
public:
    int mID ;
    float mXCoord;
    float mYCoord;
    float mPrevXCoord;
    float mPrevYCoord;
    int mLifeTime;
    int mDeathTime;
    bool mOnset;
    
    std::vector< ClusteredPoint > mCPoints;
    bool mStable;
    ofColor mColor;
};

#pragma mark CentroidGroup definition

class CentroidGroup
{
public:
    CentroidGroup();
    CentroidGroup( float pMinXPos, float pMaxXPos, int pMinCentroidID, int pMaxCentroidID );
    CentroidGroup( const CentroidGroup& pCentroidGroup );
    ~CentroidGroup();
    
    CentroidGroup& operator=( const CentroidGroup& pCentroidGroup );
    
    std::vector<Centroid> getClusters() const;
    
    inline float minXCoord() const { return mMinXCoord; }
    inline float maxXCoord() const { return mMaxXCoord; }
    
    void resetTouchPoints();
    void addTouchPoint( point* pTPoint );
    
    void calculateClusters();
    
    void display();
    
protected:
    static int sMaxCentroidCount;
    static float sMaxMatchDistance;
    
    std::list< Centroid* > mCentroids;
    std::list< Centroid > mTmpCentroids;
    std::vector< ClusteredPoint > mCPoints;
    std::deque< int > mAvailableCentroidIds;
    
    bool mAllClustersStable;
    bool mAllPointsClustered;
    bool mClusteringFinished;
    float mMinXCoord;
    float mMaxXCoord;
    int mMinCentroidID;
    int mMaxCentroidID;
    
    void updateCentroids();
    
    bool mLock = false;
};


#endif
