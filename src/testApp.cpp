#include "testApp.h"

// for random
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


//#define DRAW_BLOBS

//--------------------------------------------------------------
void testApp::setup(){
    
    ofBackground(34,34,34);
    ofSetFrameRate(50);
    ofSetVerticalSync(false);
    ofSetEscapeQuitsApp(false);
    ofEnableAlphaBlending();
    // ofSetCircleResolution(100);
    imageShader.load("images/noise.png");
    
    font.load("type/verdana.ttf", 100, true, false, true, 0.4, 72);
    

    shaderAnim.setParameters(easinglinear, ofxTween::easeIn, 0, 1, 500, 0);
    shaderShake.setParameters(easingback, ofxTween::easeInOut, 0.1, 0.3, 1000, 0);

//    shaderAnim.setParameters(1,easingback,ofxTween::easeOut,0,ofGetWidth()-100,duration,delay);


//    shader.load("shaders/electron.vert", "shaders/electron.frag");
//    shader.load("shaders/noise.vert", "shaders/noise.frag");
//    shader.load("shaders/butterfly.vert", "shaders/butterfly.frag");
    shader.load("shaders/combined.vert", "shaders/combined.frag");
    
    shader.begin();
    
    shader.setUniform1f("SEA_HEIGHT", 0.8f);
    shader.setUniform1f("SEA_CHOPPY", 30.0f);
    shader.setUniform3f("SEA_WATER_COLOR", 0.1f,0.1f,0.6f);
    shader.setUniform1f("SEA_SPEED", 3.5f);
    shader.setUniform1f("SEA_FREQ", 0.16f);
    shader.setUniform1f("CAMERA_SPEED", 0.1f);
    shader.setUniform3f("SEA_BASE", 0.1f,0.1f,0.1f);

    shader.end();
    
//    shader.load("shaders/clouds.vert", "shaders/clouds.frag");

    
    //!shader
    
    //video
    //mPlayer.load("movies/Autofahrt-Complete_GEAR-MASTER-Short_Clean_2015-09-05.mp4");
    //mPlayer.setLoopState(OF_LOOP_NORMAL);
    //mPlayer.play();
    //!video
    
    mFbo.allocate(4*1280, 720, GL_RGBA);
    mFbo.begin();
    ofClear(255,255,255, 0);
    mFbo.end();
    
    mTex.allocate(4*1280, 720, GL_RGBA);
    syphonOutTexture.setName("touchDebugger");
    
    
    setupOSC();
    setupMesh();
    doShader = true;
    
    // clustering
    float cGroupMinXPos, cGroupMaxXPos;
    for(int i=0; i<centroidGroupCount; ++i)
    {
        cGroupMinXPos = static_cast<float>(i) * 1.0;
        cGroupMaxXPos = cGroupMinXPos + 1.0;
        centroidGroups[i] = CentroidGroup( cGroupMinXPos, cGroupMaxXPos, i*200, i*200 + 199 );
    }
    
}

//--------------------------------------------------------------
void testApp::update()
{

    // Animation Timer;
//    shader.setUniform3f("SEA_WATER_COLOR", 0.8f,0.1f,0.6f);


    if (ofGetElapsedTimeMillis() > (tNow + fAnimInterval)){
        bAnimationLock = false;
//        ofLog(OF_LOG_ERROR, "ending animation");
        
        // Reset anims
        shaderAnim.setParameters(easinglinear, ofxTween::easeIn, 0, 1, 500, 0);
        shaderShake.setParameters(easinglinear, ofxTween::easeInOut, 0.1, 0.2, 200, 0);


    }
    
    // each x updates substract a value, sends blobs
    sendToMax("count", blobCount);

    if (blobCount > 0){
        blobCount = blobCount - 20;
    }
    

    
    //mPlayer.update();
    //mFbo.readToPixels(fboPixels);
    //image.setFromPixels(fboPixels);
    
    
    double now = ofGetElapsedTimef();
    double runningSeconds = now - startTime;
    
    checkForOscMessages();
    
}

//--------------------------------------------------------------
void testApp::draw()
{
    
    if( mTotalLock == true ) return;
    mTotalLock = true;
    
    iPrevCursorX = iPrevCursorX * 0.8 + iCursorX * 0.2;
    iPrevCursorY = iPrevCursorY * 0.8 + iCursorY * 0.2;
    
    
    //    mFbo.begin();
    drawShader();
    displayClusters();
    
    //    ofBackground(0, 0, 0);
    //    mPlayer.draw(-700, -600);
    //    mPlayer.draw(0, 0);
    //
    //    // drawing the blobs
    //    ofNoFill();
    //    for(int i = 0; i < MAX_BLOBS; i++) { // loop running the blobs
    //
    //        if(blobs[i].active == true) { // we have a live one
    //            ofSetColor(255, 255, 127, 255);
    //            ofSetLineWidth(2.0);
    //            ofSetCircleResolution(50);
    //            ofCircle( blobs[i].x*1280, blobs[i].y*720, 6);
    //            ofSetColor(255, 255, 255, 192);
    //
    //            ofDrawBitmapString(ofToString(i) + " " + ofToString(blobs[i].x) + " " + ofToString(blobs[i].y), blobs[i].x*1280+20, blobs[i].y*720+10);
    //        }
    //    }
    
    //    mFbo.end();
    //    mFbo.draw(0,0);
    
    
    //    ofPushMatrix(); //Store the coordinate system
    //Move the coordinate center to screen's center
    //    ofTranslate( ofGetWidth()/2, ofGetHeight()/2, 0 );
    
    //    image.bind();
    //    mesh.draw();
    //    image.unbind();
    
//    mTex.loadScreenData(0, 0, ofGetWidth(), ofGetHeight());
//    ofSetColor(255, 255, 255, 255);
//    ofEnableAlphaBlending();
//    syphonOutTexture.publishTexture(&mTex);
//    mFbo.getTexture().getTextureData().bFlipTexture = true;
//    syphonOutTexture.publishTexture(&mFbo.getTexture());
    syphonOutTexture.publishScreen();
    
    
    mTotalLock = false;
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){
    
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    
    toMax.clear();
    toMax.setAddress( "/MaxBlob" );
    toMax.addFloatArg(x);
    toMax.addFloatArg(y);
    oscSender[2].sendMessage( toMax );
    

    
    
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){
    
}

void testApp::drawShader(){
    
//    ofLog(OF_LOG_ERROR, "y %f", iPrevCursorY/float(ofGetHeight()));
    ofSetColor(245, 58, 135);
    ofFill();
    
    if( doShader ){
        shader.begin();
        //we want to pass in some varrying values to animate our type / color
        shader.setUniform1f("timeValX", ofGetElapsedTimef() * 0.1 );
        shader.setUniform1f("timeValY", -ofGetElapsedTimef() * 0.18 );
        shader.setUniform3f("iResolution", ofGetWidth(), ofGetHeight(), 0.0);
        shader.setUniform1f("iGlobalTime", ofGetElapsedTimef());
        shader.setUniform3f("resolution", ofGetWidth(), ofGetHeight(), 0.0);
        // shader.setUniform2f("iMouse", ((iPrevCursorX/4)/float(ofGetWidth())) * 2.0, iPrevCursorY/float(ofGetHeight()));
        shader.setUniform2f("iMouse", (iPrevCursorX/float(ofGetWidth())) * 2.0, (iPrevCursorY)/float(ofGetHeight()) *-0.1);
        // shader.setUniform1f("time", ofGetElapsedTimef());
        shader.setUniform1f("iRotation", iRotation/360.0f);
        // shader.setUniform1f("iGlobalTime", ofGetElapsedTimef());
        // shader.setUniform1f("SEA_HEIGHT",  mouseX / float(ofGetWidth()));
        
        //if (bAnimationLock){
        //        ofLog(OF_LOG_ERROR, "shaderAnim %f", shaderAnim.update());
        //            shader.setUniform3f("SEA_WATER_COLOR", shaderAnim.update(),0.1f,0.6f);
        //            shader.setUniform1f("SEA_HEIGHT",  shaderAnim.update());
        //            shader.setUniform1f("SEA_FREQ", shaderShake.update());
        //        shader.setUniform1f("SEA_HEIGHT", 4.0f);
        //}



        shader.setUniform1f("SEA_SPEED", (iPrevCursorX/float(ofGetWidth())));
        //shader.setUniform4f("iMouse", draggedX, draggedY, clickX, clickY);
        shader.setUniform4f("iDate", ofGetYear(), ofGetMonth(), ofGetDay(), ofGetSeconds());
        shader.setUniformTexture("iChannel0", imageShader, 1);
        //we also pass in the mouse position
        //we have to transform the coords to what the shader is expecting which is 0,0 in the center and y axis flipped.
        //        shader.setUniform2f("mouse", mouseX - ofGetWidth()/2, ofGetHeight()/2-mouseY );
        
        //        for(int i = 0; i < MAX_BLOBS; i++) { // loop running the blobs
        //
        //            if(blobs[i].active == true) { // we have a live one
        //                //                shader.setUniform2f("mouse", blobs[i].x *1280 - ofGetWidth()/2, ofGetHeight()/2- blobs[i].y*720 );
        //
        //                shader.setUniform2f("mouse", blobs[i].x *1280 - (ofGetWidth()/2), (ofGetHeight()/2) - blobs[i].y*720 );
        //            }
        //        }
        //        shader.setUniform2f("mouse", blobs[0].x - ofGetWidth()/2, ofGetHeight()/2-blobs[0].y );
        
    }
    
    //finally draw our text
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    
    if( doShader ){
        shader.end();
    }
    
    //ofDrawSphere(mouseX, mouseY, 50);
    
}

void testApp::setupMesh(){
    
    //Set up vertices
    for (int y=0; y<H; y++) {
        for (int x=0; x<W; x++) {
            mesh.addVertex(ofPoint((x - W/2) * meshSize, (y - H/2) * meshSize, 0 )); // adding texure coordinates allows us to bind textures to it later // --> this could be made into a function so that textures can be swapped / updated
            mesh.addTexCoord(ofPoint(x * (videoWidth / W), y * (videoHeight / H)));
            mesh.addColor(ofColor(255, 255, 255));
        }
    }
    
    //Set up triangles' indices
    for (int y=0; y<H-1; y++) {
        for (int x=0; x<W-1; x++) {
            int i1 = x + W * y;
            int i2 = x+1 + W * y;
            int i3 = x + W * (y+1);
            int i4 = x+1 + W * (y+1);
            mesh.addTriangle( i1, i2, i3 );
            mesh.addTriangle( i2, i4, i3 );
        }
    }
}
void testApp::setupOSC(){
    
    //	sim.start();
    //	sim.timeToLive = 20;
    
    // panelMapper
    sendIP[0] = "224.0.0.1";
    sendPort[0] = 64000;
    
    // masterTracker
    sendIP[1] = "224.0.0.1";
    sendPort[1] = 8400;
    
    // audioengine
    sendIP[2] = "224.0.0.1";
    sendPort[2] = 7777;
    
    oscSender[0].setup(sendIP[0], sendPort[0]);
    oscSender[1].setup(sendIP[1], sendPort[1]);
    oscSender[2].setup(sendIP[2], sendPort[2]);
    //	sim.oscSender = &oscSender[2];
    
    receivePort = 60001;
    oscReceiver.setup(receivePort);
    startTime = ofGetElapsedTimef();
    
    // trackerMaster init
    ofxOscMessage n;
    n.setAddress( "/trackerMaster/requestTuiostream" );
    n.addIntArg( receivePort );
    oscSender[0].sendMessage( n );
    
    // pannelmapper init
    ofxOscMessage m;
    m.setAddress( "/SwitchSyphonClient" );
    m.addStringArg( "sample-immersivelab" );
    m.addStringArg( "touchDebugger" );
    m.addFloatArg( 1.0 );
    oscSender[1].sendMessage( m );
    
    // audio engine init
    toMax.setAddress( "/MaxBlob" );
    toMax.addStringArg( "start" );
    //    toMax.addFloatArg( 1.0 );
    //    toMax.addFloatArg( 2.0 );
    oscSender[2].sendMessage( toMax );
    
    
    minimumAge  = 5.0;
    
    time_t t;
    srandom( (unsigned) time(&t));  /* Seed the PRNG */
    
    
}


void testApp::sendToMax(string param, float x, float y){
    
    std::stringstream ss;
    
    ss << "/" << param;
    
    toMax.clear();
    toMax.setAddress( ss.str() );
    toMax.addFloatArg(x);
    toMax.addFloatArg(y);
    oscSender[2].sendMessage( toMax );
    
}


void testApp::sendToMax(string param, int bid, float x, float y){

    std::stringstream ss;
    
    ss << "/" << param;
    
    toMax.clear();
    toMax.setAddress( ss.str() );
    toMax.addInt32Arg(bid);
    toMax.addFloatArg(x);
    toMax.addFloatArg(y);
    oscSender[2].sendMessage( toMax );
    
}

void testApp::sendToMax(string param, float c){
    
    std::stringstream ss;
    
    ss << "/" << param;
    
    toMax.clear();
    toMax.setAddress( ss.str() );
    toMax.addFloatArg(c);
    oscSender[2].sendMessage( toMax );
    
}

void testApp::checkForOscMessages()
{
    long i;
    int tempInt, ID;
    string tempStr;
    string oscString;
    
    while( oscReceiver.hasWaitingMessages() ) {
        ofxOscMessage m;
        oscReceiver.getNextMessage( &m );
        oscString = m.getAddress();
        
        
        if ( !strcmp( oscString.c_str(), "/tuio/2Dcur")) {
            //			cout << "OSC msg received" << endl;
            parseTuio(m); // go do the your tuio thing
        }
    }
}

void testApp::parseTuio(ofxOscMessage m)
{
    if( mTotalLock == true ) return;
    mTotalLock = true;
    
    long i, j, k;
    long blobID = -1;
    double now = ofGetElapsedTimef();
    //	point t;
    //	t.x = t.y = t.z = 0.0;
    
    string tempStr = m.getArgAsString( 0 );
    int numArgs = m.getNumArgs();
    
#pragma mark	alive
    if(!strcmp(tempStr.c_str(), "alive")) {
        
        fseqStatus = false; // check for empty frames
        
        if(numArgs > 1) {
            //			for(i = 0; i < MAX_BLOBS; i++) {
            //				blobStatus[i] = 0;
            //			}
            maxBlobAlive = -1;
            for(i = 1; i < numArgs; i++) {
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    blobID = m.getArgAsInt32(i);
                    blobID = blobID % MAX_BLOBS;
                    
                    if(blobs[blobID].active == false) { // a new blob is born
                        blobs[blobID].age = now;
                        blobs[blobID].active = true;
                        blobs[blobID].deathTime = 0;
                        //						blobs[blobID].rect.setX(blobs[blobID].x);
                        //						blobs[blobID].rect.setY(blobs[blobID].y);
                        blobs[blobID].canTrigger = true; // the first appearance needed for storage
                    }
                    blobStatus[blobID] = 1;	// set tempblob flags
                    if(maxBlobAlive < blobID) {
                        maxBlobAlive = blobID; // store largest blob alive
                    }
                }
            }
            // check which ones have gone
            for(i = 0; i < MAX_BLOBS; i++){
                if(blobStatus[i] == false && blobs[i].active == true) {
                    
                    
                    if( blobs[i].deathTime > blobs[i].maxDeathTime  )
                    {
                        blobs[i].active = false;
                        blobs[i].newTrigger = true;
                        blobs[i].age = -1.0;
                    }
                    
                    if(blobs[i].age > minimumAge) { // minimum age reached we can trigger now
                        if( blobs[i].rect.width < 0.0){
                            selectedDirection[i] = -1.;
                        }else{
                            selectedDirection[i] = 1;
                        }
                        double interval = blobs[i].age * 0.005;
                        double centre_y = (blobs[i].rect.y * 720);
                        
                        
                        uint64 R = random();  /* Generate a random integer */
                        double stepSize = ((R % 700) + 200) * 0.0001; // scale to max-range of invTable
                        
                        double dimensions_x = blobs[i].rect.width * 1280;
                        double dimensions_y = blobs[i].rect.y * ((R % 700)+20);
                        
                        
                        int direction = selectedDirection[i];
                        double anglePosition = blobs[i].rect.x * 90.0;
                        anglePosition =  fmod(anglePosition, 360.0);
                        
                        //						cout << "blob:  age " << blobs[i].age << " rect.x " << blobs[i].rect.x << " width " << blobs[i].rect.width << endl;
                        
                        
                    }
                    
                }
            }
        }
    }
    
#pragma mark set
    else if(!strcmp(tempStr.c_str(), "set")) { // format: set ID x y accel-x accel-y
        
        if(numArgs > 3) {
            // blob ID
            if(m.getArgType(1) == OFXOSC_TYPE_INT32) {
                blobID = m.getArgAsInt32(1);
                blobID = blobID % MAX_BLOBS; // safety!! wrap to existing array sizes
                
                blobs[blobID].ID = blobID;
            }
            if(m.getArgType(2) == OFXOSC_TYPE_FLOAT){
                blobs[blobID].x = m.getArgAsFloat(2);
            }else if(m.getArgType(2) == OFXOSC_TYPE_INT32) {
                blobs[blobID].x = (double)m.getArgAsInt32(2);
            }
            
            if(m.getArgType(3) == OFXOSC_TYPE_FLOAT){
                blobs[blobID].y = m.getArgAsFloat(3);
            }else if(m.getArgType(3) == OFXOSC_TYPE_INT32) {
                blobs[blobID].y = (double)m.getArgAsInt32(3);
            }
            
        }
    }
    
#pragma mark	fseq
    else if(!strcmp(tempStr.c_str(), "fseq")) {
        
        int frameNr = m.getArgAsInt( 1 );
        
        if(fseqStatus == true) { // there was NO alive part in the message
            for(i = 0; i < MAX_BLOBS; i++) { // loop running the blobs
                
                if(blobs[i].active == true ) blobs[i].deathTime += 1;
                
                if(blobs[i].deathTime >= blobs[i].maxDeathTime ) blobs[i].active = false;
            }
        }
        else
        {
            for(i = 0; i < MAX_BLOBS; i++) { // loop running the blobs
                
                if(blobStatus[i] == true) { // we have a live one
                    
                    // collect min and max coords in x and y and delta between min.x and max.x for direction
                    blobCount++;
                    
                    if(blobs[i].canTrigger){ // store the beginning point
                        blobs[i].rect.setX(blobs[i].x);
                        blobs[i].rect.setY(blobs[i].y);
                        blobs[i].canTrigger = false;
                    }else{ // update the following points, thus growing the rectangle, until we reach end of blob
                        
                        blobs[i].rect.setWidth(blobs[i].x - blobs[i].rect.x);
                        blobs[i].rect.setHeight(blobs[i].y - blobs[i].rect.y);
                    }
                    //				printf("blob %ld %f %f\n", i, blobs[i].x, blobs[i].y);
                }
            } // end of running the blobs loop
            
            for(i = 0; i < MAX_BLOBS; i++) {
                blobStatus[i] = 0;
            }
            
            fseqStatus = true;
        }
        
        calculateClusters();
    } // end "fseq case"
    
    mTotalLock = false;
}



void
testApp::calculateClusters()
{
    if( mClusterLock == true ) return;
    mClusterLock = true;
    
    // reset points for clusters
    for(int i=0; i<centroidGroupCount; ++i)
    {
        centroidGroups[i].resetTouchPoints();
    }
    
    // assign active points to cluster groups
    for(int pI=0; pI<MAX_BLOBS; ++pI)
    {
        point& tPoint = blobs[pI];
        
        for(int cI=0; cI<centroidGroupCount; ++cI)
        {
            
            if( tPoint.active == true && tPoint.x >= centroidGroups[cI].minXCoord() && tPoint.x < centroidGroups[cI].maxXCoord() )
            {
                centroidGroups[cI].addTouchPoint(&tPoint);
            }
        }
    }
    
    for(int i=0; i<centroidGroupCount; ++i)
    {
        centroidGroups[i].calculateClusters();
    }
    
    mClusterLock = false;
}

void
testApp::displayClusters()
{
    
    for(int cI=0; cI<centroidGroupCount; ++cI)
    {
        std::vector<Centroid> clusters = centroidGroups[cI].getClusters();
        
        int idx = 0;
        ofEnableAlphaBlending();
        for(auto it=clusters.begin(); it != clusters.end(); ++it) {
            
            // ofDrawBitmapString(ofToString((*it).mID), (*it).mXCoord*(ofGetWidth()/4)+20, (*it).mYCoord*ofGetHeight()+10);
            ofSetColor(246, 20, 0, 3);
            ofSetPolyMode(OF_POLY_WINDING_ODD);
            ofNoFill();
            ofBeginShape();
            for (int i = 0; i < 5; i++){
                ofDrawIcoSphere(ofRandom((*it).mXCoord*(ofGetWidth()/4),(*it).mXCoord*(ofGetWidth()/4)+100), ofRandom((*it).mYCoord*ofGetHeight()),ofRandom(0.0, 50.f));
                // ofVertex(ofRandom((*it).mXCoord*(ofGetWidth()/4),(*it).mXCoord*(ofGetWidth()/4)+100), ofRandom((*it).mYCoord*ofGetHeight(),(*it).mYCoord*ofGetHeight() + 100));
                
                ofVertex(ofRandom((*it).mXCoord*(ofGetWidth()/4),(*it).mXCoord*(ofGetWidth()/4)+100), ofRandom((*it).mYCoord*ofGetHeight(),(*it).mYCoord*ofGetHeight() + 100));
            }
            ofEndShape();
            ofFill();

            
            if ((*it).mOnset){
                onHand((*it).mID, (*it).mXCoord*1280, (*it).mYCoord*720);
            }
            sendToMax("blob", idx, (*it).mXCoord*1280, (*it).mYCoord*720);
            idx++;

        }
    }
}

void testApp::onHand(int bid, float x, float y){
    
    iCursorX = x;
    iCursorY = y;

    if (x > 2560){
        iRotation++;
    }else{
        iRotation--;
    }

    // Move screen
    if (bAnimationLock == false){
    
        bAnimationLock = true;
        tNow = ofGetElapsedTimeMillis();
        shakeScreen();
        
    }
    
    bPrevState = bAlive;
    
}

void testApp::shakeScreen(){

//    ofLog(OF_LOG_ERROR, "Shaking...");
//    shaderAnim.start();

}

void testApp::getkmeans(){
    
    int sampleCount = blobsAlive.size();
    int dimensions = 2;
    float pointsdata[sampleCount*2]; //[] = {1,1, 2,2, 6,6, 5,5, 10,10};
    
    int cnt = 0;
    for(int a=0; a<sampleCount; a++){
        pointsdata[cnt] = blobs[a].x;
        cnt++;
        pointsdata[cnt] = blobs[a].y;
        cnt++;
    }
    
    cv::Mat points(sampleCount,dimensions, CV_32F,pointsdata);
    
    int clusterCount = 3; //i want 3 averaged points back
    
    cv::Mat labels;
    cv::Mat centers(clusterCount, 1, points.type());
    
    
    cv::kmeans(points, 3, labels, cv::TermCriteria(), 2 ,cv::KMEANS_PP_CENTERS, centers);
    
    cout<<"labels: "<<labels.rows<<endl;
    for(int n = 0; n < labels.rows; n++)
    {
        cout<<labels.at<int>(0, n)<<endl;
    }
    
    cout<<"\ncenters: "<<centers<<endl;
    
    
}

