/*
 ---------------------------------------------
 FROM GLSL SANDBOX TO OPENFRAMEWORKS
 ---------------------------------------------
 BY HUBRIS [http://cargocollective.com/hubris]
 [http://github.com/youandhubris]
 BERLIN · JULY 2014
 ---------------------------------------------
 BETA VERSION
 ---------------------------------------------
 */

#version 120
# define M_PI           3.14159265358979323846
// ---> Shadertoy uniforms
uniform vec3 iResolution;
uniform float iGlobalTime;
uniform vec2 iMouse;
uniform sampler2D iChannel0; // Texture #1
uniform sampler2D iChannel1; // Texture #2
uniform sampler2D iChannel2; // Texture #3
uniform sampler2D iChannel3; // Texture #4
uniform vec4 iDate;



float RED = 1.0;
float BLUE = 0.0;
float GREEN = 0.0;

uniform float SEA_HEIGHT;
uniform float SEA_CHOPPY;
uniform float SEA_SPEED;
uniform float CAMERA_SPEED;
uniform float SEA_FREQ;
uniform vec3 SEA_WATER_COLOR;
uniform vec3 SEA_BASE;
uniform float iRotation;

// --> Shadertoy code
// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// I've not seen anybody out there computing correct cell interior distances for Voronoi
// patterns yet. That's why they cannot shade the cell interior correctly, and why you've
// never seen cell boundaries rendered correctly.
//
// However, here's how you do mathematically correct distances (note the equidistant and non
// degenerated grey isolines inside the cells) and hence edges (in yellow):
//
// http://www.iquilezles.org/www/articles/voronoilines/voronoilines.htm


#define ANIMATE

const int NUM_STEPS = 8;
const float PI	 	= 3.1415;
const float EPSILON	= 1e-3;
float EPSILON_NRM	= 0.1 / iResolution.x;

// sea
/*const int ITER_GEOMETRY = 3;
const int ITER_FRAGMENT = 5;
const float SEA_HEIGHT = 0.6;
const float SEA_CHOPPY = 4.0;
const float SEA_SPEED = 0.8;
const float SEA_FREQ = 0.16;
const vec3 SEA_BASE = vec3(0.1,0.19,0.22);
const vec3 SEA_WATER_COLOR = vec3(0.8,0.9,0.6);*/



const int ITER_GEOMETRY = 2;
const int ITER_FRAGMENT = 3;
/*const float SEA_HEIGHT = 1.2;*/
/*const float SEA_CHOPPY = 80.0;*/
/*const float SEA_SPEED = 5.5;*/
/*const float SEA_FREQ = 0.16;*/
/*const vec3 SEA_WATER_COLOR = vec3(0.8,0.9,0.6);*/
/*const vec3 SEA_BASE = vec3(0.1,0.19,0.22);*/


float SEA_TIME = iGlobalTime * SEA_SPEED;
mat2 octave_m = mat2(1.6,1.2,-1.2,1.6);


// math
mat3 fromEuler(vec3 ang) {
    vec2 a1 = vec2(sin(ang.x),cos(ang.x));
    vec2 a2 = vec2(sin(ang.y),cos(ang.y));
    vec2 a3 = vec2(sin(ang.z),cos(ang.z));
    mat3 m;
    m[0] = vec3(a1.y*a3.y+a1.x*a2.x*a3.x,a1.y*a2.x*a3.x+a3.y*a1.x,-a2.y*a3.x);
    m[1] = vec3(-a2.y*a1.x,a1.y*a2.y,a2.x);
    m[2] = vec3(a3.y*a1.x*a2.x+a1.y*a3.x,a1.x*a3.x-a1.y*a3.y*a2.x,a2.y*a3.y);
    return m;
}
float hash( vec2 p ) {
    float h = dot(p,vec2(127.1,311.7));
    return fract(sin(h)*43758.5453123);
}
float noise( in vec2 p ) {
    vec2 i = floor( p );
    vec2 f = fract( p );
    vec2 u = f*f*(3.0-2.0*f);
    return -1.0+2.0*mix( mix( hash( i + vec2(0.0,0.0) ),
                             hash( i + vec2(1.0,0.0) ), u.x),
                        mix( hash( i + vec2(0.0,1.0) ),
                            hash( i + vec2(1.0,1.0) ), u.x), u.y);
}

// lighting
float diffuse(vec3 n,vec3 l,float p) {
    return pow(dot(n,l) * 0.4 + 0.6,p);
}
float specular(vec3 n,vec3 l,vec3 e,float s) {
    float nrm = (s + 8.0) / (3.1415 * 8.0);
    return pow(max(dot(reflect(e,n),l),0.0),s) * nrm;
}

// sky
vec3 getSkyColor(vec3 e) {
    /*e.y = max(e.y,0.0);
    vec3 ret;
    ret.x = pow(1.0-e.y,2.0);
    ret.y = 1.0-e.y;
    ret.z = 0.6+(1.0-e.y)*0.4;
    return ret;*/

    e.y = max(e.y,0.0);
    vec3 ret;
    ret.x = pow(1.0-e.y,4.0);
    ret.y = 0.7-e.y;
    ret.z = (1.0-e.y)*0.4;
    return ret;

}

// sea
float sea_octave(vec2 uv, float choppy) {
    uv += noise(uv);
    vec2 wv = 1.0-abs(sin(uv));
    vec2 swv = abs(cos(uv));
    wv = mix(wv,swv,wv);
    return pow(1.0-pow(wv.x * wv.y,0.65),choppy);
}

float map(vec3 p) {
    float freq = SEA_FREQ;
    float amp = SEA_HEIGHT;
    float choppy = SEA_CHOPPY;
    vec2 uv = p.xz; uv.x *= 0.75;

    float d, h = 0.0;
    for(int i = 0; i < ITER_GEOMETRY; i++) {
        d = sea_octave((uv+SEA_TIME)*freq,choppy);
        d += sea_octave((uv-SEA_TIME)*freq,choppy);
        h += d * amp;
        uv *= octave_m; freq *= 1.9; amp *= 0.22;
        choppy = mix(choppy,1.0,0.2);
    }
    return p.y - h;
}

float map_detailed(vec3 p) {
    float freq = SEA_FREQ;
    float amp = SEA_HEIGHT;
    float choppy = SEA_CHOPPY;
    vec2 uv = p.xz; uv.x *= 0.75;

    float d, h = 0.0;
    for(int i = 0; i < ITER_FRAGMENT; i++) {
        d = sea_octave((uv+SEA_TIME)*freq,choppy);
        d += sea_octave((uv-SEA_TIME)*freq,choppy);
        h += d * amp;
        uv *= octave_m; freq *= 1.9; amp *= 0.22;
        choppy = mix(choppy,1.0,0.2);
    }
    return p.y - h;
}
vec3 getSeaColor(vec3 p, vec3 n, vec3 l, vec3 eye, vec3 dist) {
    float fresnel = 1.0 - max(dot(n,-eye),0.0);
    fresnel = pow(fresnel,3.0) * 0.65;

    vec3 reflected = getSkyColor(reflect(eye,n));
    vec3 refracted = SEA_BASE + diffuse(n,l,80.0) * SEA_WATER_COLOR * 0.12;

    vec3 color = mix(refracted,reflected,fresnel);

    float atten = max(1.0 - dot(dist,dist) * 0.001, 0.0);
    color += SEA_WATER_COLOR * (p.y - SEA_HEIGHT) * 0.18 * atten;

    color += vec3(specular(n,l,eye,60.0));

    return color;
}

// tracing
vec3 getNormal(vec3 p, float eps) {
    vec3 n;
    n.y = map_detailed(p);
    n.x = map_detailed(vec3(p.x+eps,p.y,p.z)) - n.y;
    n.z = map_detailed(vec3(p.x,p.y,p.z+eps)) - n.y;
    n.y = eps;
    return normalize(n);
}

float heightMapTracing(vec3 ori, vec3 dir, out vec3 p) {
    float tm = 0.0;
    float tx = 1000.0;
    float hx = map(ori + dir * tx);
    if(hx > 0.0) return tx;
    float hm = map(ori + dir * tm);
    float tmid = 0.0;
    for(int i = 0; i < NUM_STEPS; i++) {
        tmid = mix(tm,tx, hm/(hm-hx));
        p = ori + dir * tmid;
        float hmid = map(p);
        if(hmid < 0.0) {
            tx = tmid;
            hx = hmid;
        } else {
            tm = tmid;
            hm = hmid;
        }
    }
    return tmid;
}

//********************

float makePoint(float x,float y,float fx,float fy,float sx,float sy,float t){
   float xx=x+sin(t*fx)*sx;
   float yy=y+cos(t*fy)*sy;
   return 0.54/sqrt(xx*xx+yy*yy);
}

vec3 gu(vec4 a,vec4 b,float f){
  return mix(a.xyz,b.xyz,(f-a.w)*(1.0/(b.w-a.w)));
}

vec3 grad(float f){
	vec4 c01=vec4(0.0,0.0,0.0,0.00);
	vec4 c02=vec4(RED*0.5,BLUE*0.1,GREEN*0.1,0.50);
	vec4 c03=vec4(RED,BLUE*0.3,GREEN*0.3,0.55);
	vec4 c04=vec4(RED,BLUE*0.4,GREEN*0.4,0.80);
	vec4 c05=vec4(RED,BLUE*0.6,GREEN*0.6,1.00);
	return (f<c02.w)?gu(c01,c02,f):
	(f<c03.w)?gu(c02,c03,f):
	(f<c04.w)?gu(c03,c04,f):
	gu(c04,c05,f);
}

//********************

void main( void )
{
    vec2 uv = gl_FragCoord.xy/iResolution.xy;
    vec2 pi = (gl_FragCoord.xy/iResolution.x)*2.0-vec2(iMouse.x,iMouse.y);
    pi = pi*2.0;

    RED = normalize((sin(iGlobalTime) + 2.0)/2.0);
    BLUE = normalize(cos(iGlobalTime));
    GREEN = normalize(cos(iGlobalTime));
    /*GREEN = 1.0;*/
    /*BLUE  = 1.0*/

    uv = uv * 2.0 - 1.0;
    uv.x *= iResolution.x / iResolution.y;
    float time = iGlobalTime * 1.4;

    //PointSize
    float x=pi.x;
    float y=pi.y;

    float a=
        makePoint(x,y,3.3,2.9,0.3,0.3,time);
    a=a+makePoint(x,y,1.9,2.0,0.4,0.4,time);
    a=a+makePoint(x,y,0.8,0.7,0.4,0.5,time);
    a=a+makePoint(x,y,2.3,0.1,0.6,0.3,time);
    a=a+makePoint(x,y,0.8,1.7,0.5,0.4,time);
    a=a+makePoint(x,y,0.3,1.0,0.4,0.4,time);
    a=a+makePoint(x,y,1.4,1.7,0.4,0.5,time);
    a=a+makePoint(x,y,1.3,2.1,0.6,0.3,time);
    a=a+makePoint(x,y,1.8,1.7,0.5,0.4,time);
    a=a+makePoint(x,y,1.2,1.9,0.3,0.3,time);
    a=a+makePoint(x,y,0.7,2.7,0.4,0.4,time);
    a=a+makePoint(x,y,1.4,0.6,0.4,0.5,time);
    a=a+makePoint(x,y,2.6,0.4,0.6,0.3,time);
    a=a+makePoint(x,y,0.7,1.4,0.5,0.4,time);
    a=a+makePoint(x,y,0.7,1.7,0.4,0.4,time);
    a=a+makePoint(x,y,0.8,0.5,0.4,0.5,time);
    a=a+makePoint(x,y,1.4,0.9,0.6,0.3,time);
    a=a+makePoint(x,y,0.7,1.3,0.5,0.4,time);
    a=a+makePoint(x,y,3.7,0.3,0.3,0.3,time);
    a=a+makePoint(x,y,1.9,1.3,0.4,0.4,time);
    a=a+makePoint(x,y,0.8,0.9,0.4,0.5,time);
    a=a+makePoint(x,y,1.2,1.7,0.6,0.3,time);
    a=a+makePoint(x,y,0.3,0.6,0.5,0.4,time);
    a=a+makePoint(x,y,0.3,0.3,0.4,0.4,time);
    a=a+makePoint(x,y,1.4,0.8,0.4,0.5,time);
    a=a+makePoint(x,y,0.2,0.6,0.6,0.3,time);
    a=a+makePoint(x,y,1.3,0.5,0.5,0.4,time);
    a=a+makePoint(x,y,0.7,1.3,0.5,0.4,time);
    a=a+makePoint(x,y,3.7,0.3,0.3,0.3,time);
    a=a+makePoint(x,y,1.9,1.3,0.4,0.4,time);
    a=a+makePoint(x,y,0.8,0.9,0.4,0.5,time);
    a=a+makePoint(x,y,1.2,1.7,0.6,0.3,time);
    a=a+makePoint(x,y,0.3,0.6,0.5,0.4,time);
    a=a+makePoint(x,y,0.3,0.3,0.4,0.4,time);
    a=a+makePoint(x,y,1.4,0.8,0.4,0.5,time);
    a=a+makePoint(x,y,0.2,0.6,0.6,0.3,time);
    a=a+makePoint(x,y,1.3,0.5,0.5,0.4,time);

    vec3 a1=grad(a/325.0);

    // ray
    /*vec3 ang = vec3(sin(time*1.0)*0.1,sin(time)*0.2+0.3,time);*/
    vec3 ang = vec3(0,0.1*sin(time*1.0),iRotation);
    vec3 ori = vec3(0.0,3.3,100*sin(time*CAMERA_SPEED));
    vec3 dir = normalize(vec3(uv.xy,-2.0));
    dir.z += length(uv) * 0.15;
    dir = normalize(dir) * fromEuler(ang);

    // tracing
    vec3 p;
    heightMapTracing(ori,dir,p);
    vec3 dist = p - ori;
    vec3 n = getNormal(p, dot(dist,dist) * EPSILON_NRM);
    vec3 light = normalize(vec3(0.0,1.0,0.8));

    // color
    vec3 color = mix(
                     (getSkyColor(dir)*1.8)*(a1*0.5)+0.01,
                     (getSeaColor(p,n,light,dir,dist)*1.8)*(a1*2.3)+0.01,
                     pow(smoothstep(0.0,-0.09,dir.y),0.3));

    gl_FragColor = vec4(pow(color,vec3(0.6)), 1.5);
    /*gl*/
    /*gl_FragColor = vec4(a1.x,a1.y,a1.z,1.0);*/


}
